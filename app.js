const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);
const playlist = $('.playlist');
const cd = $('.cd');
const heading = $('header h2');
const cdThumd = $('.cd-thumb');
const audio = $('#audio');
const playBtn = $('.btn-toggle-play');
const player = $('.player')
const progress = $('#progress')
const nextBtn = $('.btn-next')
const prevBtn = $('.btn-prev')
const randomBtn = $('.btn-random')
const repeatBtn = $('.btn-repeat')



const app = {
  currentIndex: 0,
  isPlaying: false,
  isRandom: false,
  isRepeat :false,
    songs :  [
        {
            name: "Người lạ thoáng qua",
            singer: "Hoài Lâm cover",
            path: "./music/Nguoi-La-Thoang-Qua-Dinh-Tung-Huy-ACV.mp3",
            image: "./img/channels4_profile.jpg"
          },
          {
            name: "Tu Phir Se Aana",
            singer: "Raftaar x Salim Merchant x Karma",
            path: "https://mp3.vlcmusic.com/download.php?track_id=34213&format=320",
            image:
              "https://1.bp.blogspot.com/-kX21dGUuTdM/X85ij1SBeEI/AAAAAAAAKK4/feboCtDKkls19cZw3glZWRdJ6J8alCm-gCNcBGAsYHQ/s16000/Tu%2BAana%2BPhir%2BSe%2BRap%2BSong%2BLyrics%2BBy%2BRaftaar.jpg"
          },
          {
            name: "Naachne Ka Shaunq",
            singer: "Raftaar x Brobha V",
            path:
              "https://mp3.filmysongs.in/download.php?id=Naachne Ka Shaunq Raftaar Ft Brodha V Mp3 Hindi Song Filmysongs.co.mp3",
            image: "https://i.ytimg.com/vi/QvswgfLDuPg/maxresdefault.jpg"
          },
          {
            name: "Mantoiyat",
            singer: "Raftaar x Nawazuddin Siddiqui",
            path: "https://mp3.vlcmusic.com/download.php?track_id=14448&format=320",
            image:
              "https://a10.gaanacdn.com/images/song/39/24225939/crop_480x480_1536749130.jpg"
          },
          {
            name: "Aage Chal",
            singer: "Raftaar",
            path: "https://mp3.vlcmusic.com/download.php?track_id=25791&format=320",
            image:
              "https://a10.gaanacdn.com/images/albums/72/3019572/crop_480x480_3019572.jpg"
          },
          {
            name: "Feeling You",
            singer: "Raftaar x Harjas",
            path: "https://mp3.vlcmusic.com/download.php?track_id=27145&format=320",
            image:
              "https://a10.gaanacdn.com/gn_img/albums/YoEWlabzXB/oEWlj5gYKz/size_xxl_1586752323.webp"
          }
        ],
        render: function(){
          const htmls = this.songs.map((song,index )=>{
              return `
              <div class="song ${index=== this.currentIndex ? 'active ': ''}"data-index="${index}">
              <div class="thumb"
                  style="background-image: url('${song.image}')">
              </div>
              <div class="body">
                  <h3 class="title">${song.name}</h3>
                  <p class="author">${song.singer}</p>
              </div>
              <div class="option">
                  <i class="fas fa-ellipsis-h"></i>
              </div>
          </div>
              `
          })
          // in ra màn hình 
          playlist.innerHTML = htmls.join('');

      },
        handleEvents : function(){
          const _this = this;
          // tạo sự kiện kéo thanh scroll
          const cdwidth = cd.offsetWidth;
          // xử lý cd quay và dưng
          const cdThumbAnimate = cdThumd.animate([
            {transform:'rotate(360deg)'}
          ],{
            duration:10000,
            iterations: Infinity
          })
          cdThumbAnimate.pause();

          document.onscroll = function(){
            const scrolltop = window.scrollY || document.documentElement.scrollTop;
            const newcdwwidth = cdwidth - scrolltop;
            cd.style.width = newcdwwidth  > 0 ? newcdwwidth + 'px' : 0;
            cd.style.opacity = newcdwwidth / cdwidth
          }
          // xử lý play và pause
          playBtn.onclick = function(){
            if(_this.isPlaying){
              audio.pause()
            }else{
              audio.play()
            }
          }
          // khi song được play
          audio.onplay = function(){
            _this.isPlaying = true
            player.classList.add('playing')
            cdThumbAnimate.play()
          }
          // khi song bị pause
          audio.onpause = function(){
            _this.isPlaying = false
            player.classList.remove('playing')
            cdThumbAnimate.pause()
          }
          // khi tiến độ bài hát thay đổi
          audio.ontimeupdate = function(){
            if(audio.duration){
              const progresspercent = Math.floor(audio.currentTime/audio.duration *100)
              progress.value = progresspercent
            }
            
          }
          // xử lý tua bài hát
          progress.onchange = function(e){
            const seekTime = audio.duration *e.target.value / 100 ;
            audio.currentTime = seekTime;
          }
          nextBtn.onclick = function(){
            if(_this.isRandom){
              _this.playRandomSong()
            }else{
              _this.nextSong();
            }
            audio.play();
            _this.scrollToActiveSong()
            _this.render()
          }
          prevBtn.onclick = function(){
            if(_this.isRandom){
              _this.playRandomSong()
            }else{
              _this.prevSong()
            }
            _this.render()
            audio.play();
            _this.scrollToActiveSong()
          }
          randomBtn.onclick = function(e){
            _this.isRandom = !_this.isRandom
            randomBtn.classList.toggle('active',_this.isRandom)
          }
          // xử lý phát lại bài hát
          repeatBtn.onclick = function(e){
            _this.isRepeat = !_this.isRepeat
            repeatBtn.classList.toggle('active',_this.isRepeat)

          }
          // sựu kiện khi bài hất hết
          audio.onended = function(){
            if(_this.isRepeat){
              audio.play()
            }else{
              nextBtn.click();

            }
          }
          playlist.onclick = function (e) {
            const songNode = e.target.closest(".song:not(.active)");
      
            if (songNode || e.target.closest(".option")) {
              // Xử lý khi click vào song
              // Handle when clicking on the song
              if (songNode) {
                _this.currentIndex = Number(songNode.dataset.index);
                _this.loadcurrentSongs();
                _this.render();
                audio.play();
              }
      
              // Xử lý khi click vào song option
              // Handle when clicking on the song option
              if (e.target.closest(".option")) {
              }
            }
          };
        },
       
        defineProperties: function(){
          Object.defineProperty(this,'currentSong',{
            get: function(){
              return this.songs[this.currentIndex]
            }
          })
        },
        scrollToActiveSong:function(){
          setTimeout(()=>{
              $('.song.active').scrollIntoView({
                  behavior: "smooth",
                  block: "center"
                })
          },300)
      },
        loadcurrentSongs: function(){
          heading.textContent = this.currentSong.name;
          cdThumd.style.backgroundImage = `url('${this.currentSong.image}')`;
          audio.src = this.currentSong.path;
        },
        nextSong: function(){
          this.currentIndex++
          if(this.currentIndex  >= this.songs.length){
              this.currentIndex = 0
          }
          this.loadcurrentSongs()
      },
      prevSong: function(){
        this.currentIndex--
        if(this.currentIndex  < 0){
            this.currentIndex =  this.songs.length - 1
        }
        this.loadcurrentSongs()
    }, 
       playRandomSong: function(){
      let newIndex;
      do{
          newIndex = Math.floor(Math.random() * this.songs.length)
      }while(newIndex === this.currentIndex);
      this.currentIndex = newIndex
      this.loadcurrentSongs()
  },


        start: function(){
          // xử lý cái sựu kiện dom
          this.handleEvents();
          //render bài hát
          this.render();
          //định nghĩa các thuộc tính cảu object
          this.defineProperties()
          // tải bài hát hiện tại vào Ui
          this.loadcurrentSongs()
        }
}
app.start();